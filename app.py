import cherrypy

cherrypy.server.socket_host = '0.0.0.0'

class HelloWorld(object):
    @cherrypy.expose
    def index(self):
        return "Hello World! from CherryPy v2 :D"

cherrypy.quickstart(HelloWorld())
